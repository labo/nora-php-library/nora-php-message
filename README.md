# Message Servide

```php
$srv = MessageServiceBuilder::build([
    'MESSAGE.000001.D' => "デバッグメッセージ",
    'MESSAGE.000002.I' => "インフォメーションメッセージ",
    'MESSAGE.000003.W' => "警告です。%{内容}",
    'MESSAGE.999999.D' => "メッセージコード %{_CODE}",
]);

$this->assertEquals("デバッグメッセージ", $srv->buildMessage('MESSAGE.000001'));
$this->assertEquals("警告です。中身", $srv->buildMessage('MESSAGE.000003', [
    '内容' => '中身'
]));
$this->assertEquals("メッセージコード MESSAGE.999999", $srv->buildMessage('MESSAGE.999999'));

$log = [];
$handle = $srv->withHandler(new class($log) extends AbstractMessageHandler {
    public function __construct(&$log) {
        $this->log =& $log;
    }
    public function onDebug($m) 
    {
        $this->log[] = sprintf('[%s] %s', $m->getPriorityName(), (string) $m);
    }

    public function onInfo($m) 
    {
        $this->log[] = sprintf('[%s] %s', $m->getPriorityName(), (string) $m);
    }
    public function onWarning($m) 
    {
        $this->log[] = sprintf('[%s] %s', $m->getPriorityName(), (string) $m);
    }
});

$handle->pushMessage('MESSAGE.999999');
$handle->pushMessage('MESSAGE.000003', ['内容' => 'なし']);
$handle->pushMessage('MESSAGE.000001');

$this->assertEquals('[warning] 警告です。なし', $log[1]);
```

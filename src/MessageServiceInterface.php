<?php
declare(strict_types=1);

namespace Nora\Message;

interface MessageServiceInterface
{
    public function buildMessage($code, $params = []) : MessageInterface;
    public function withHandler(MessageHandlerInterface $handler): MessageInterface;
    public function pushMessage($code, $params = []) : MessageInterface;
    public function sendMessage(MessageInterface $message) : MessageInterface;
}

<?php
declare(strict_types=1);

namespace Nora\Message;

class Message implements MessageInterface
{
    public $body;
    public $priority;
    private static $priorityMap = [
        'D' => 'debug',
        'I' => 'info',
        'W' => 'warning',
        'F' => 'fatal',
        'C' => 'critical',
        'A' => 'alert',
        'E' => 'emerge'
    ];

    public function __construct(string $body, string $priority, array $context = [])
    {
        $this->priority = $priority;
        $this->body = $body;
        $this->context = $context;
    }

    public function getPriorityName()
    {
        return static::$priorityMap[$this->priority] ?? $this->priority;
    }

    public function __toString()
    {
        return $this->body;
    }
}

<?php
declare(strict_types=1);

namespace Nora\Message;

class MessageFormatStorage
{
    private $format = [];

    public static function create(...$args)
    {
        $storage = new self();
        foreach($args as $arg){
            if (is_array($arg)){
                $storage->setFromArray($arg);
                continue;
            }
            $storage->set($key, $value);
        }
        return $storage;
    }

    protected function setFromArray(array $array)
    {
        foreach($array as $code => $message){
            $this->set($code, $message);
        }
    }

    public function set($code, $message)
    {
        $info = explode(".", $code);
        $this->format[$info[0]][$info[1]] = [
            'message' => $message,
            'priority' => $info[2]
        ];
    }

    public function format($code, $params) : array
    {
        $info = explode(".", $code);
        if (!isset($this->format[$info[0]])) {
            throw new \InvalidArgumentException("{$info[0]} is not defined with Context {$info[1]}");
        }
        if (!isset($this->format[$info[0]][$info[1]])) {
            throw new \InvalidArgumentException("{$info[0]}.{$info[1]} is not defined");
        }
        $info = $this->format[$info[0]][$info[1]];

        $params = array_merge([
            '_CODE' => $code
        ], $params);

        $message = preg_replace_callback('/\%\{([^}]+)\}/', function($m) use ($params) {
            if (!isset($params[$m[1]])) {
                return '%{'.$m[1].': not assigned'.'}';
            }
            return $params[$m[1]];
        }, $info['message']);
        return [
            'body' => $message,
            'priority' => $info['priority']
        ];
    }
}

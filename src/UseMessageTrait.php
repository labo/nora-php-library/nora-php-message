<?php
declare(strict_types=1);

namespace Nora\Message;

use Nora\Architecture\DI\Annotation\Inject;

trait UseMessageTrait
{
    private $log = [];
    private $message;

    /**
     * @Inject
     */
    public function setMessageService(MessageServiceInterface $message)
    {
        $this->message = $message;
    }

    /**
     * メッセージを追加する
     */
    protected function pushMessage(string $code, array $params = [])
    {
        return $this->log[] = $this->message->pushMessage($code, $params);
    }

    /**
     * メッセージを取得する
     */
    protected function getLog() : array
    {
        return $this->log;
    }
}

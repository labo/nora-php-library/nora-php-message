<?php
declare(strict_types=1);

namespace Nora\Message;

use PHPUnit\Framework\TestCase;

abstract class AbstractMessageHandler implements MessageHandlerInterface
{
    public function handle(MessageInterface $message)
    {
        if (method_exists($this, 'onMessage')) {
            $this->onMessage($message);
        }
        $name = $message->getPriorityName();

        if (method_exists($this, 'on'.ucfirst($name))) {
            $this->{'on'.ucfirst($name)}($message);
        }
    }

    public function onMessage(MessageInterface $message)
    {
    }
}

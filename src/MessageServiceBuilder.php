<?php
declare(strict_types=1);

namespace Nora\Message;

class MessageServiceBuilder
{
    public static function build($spec)
    {
        $storage = new MessageFormatStorage();
        foreach ($spec as $code => $message) {
            $storage->set($code, $message);
        }
        return new MessageService($storage);
    }
}

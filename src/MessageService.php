<?php
declare(strict_types=1);

namespace Nora\Message;

class MessageService implements MessageServiceInterface
{
    private $format;
    private $handler = [];

    public function __construct(MessageFormatStorage $format, array $handler = [])
    {
        $this->format = $format;
        $this->handler = $handler;
    }

    public function buildMessage($code, $params = []) : MessageInterface
    {
        $message = $this->format->format($code, $params);
        $params = array_merge([
            '_code' => $code
        ], $params);
        return new Message($message['body'], $message['priority'], $params);
    }

    public function withHandler(MessageHandlerInterface $handler) : MessageInterface
    {
        return new static($this->format, [$handler]);
    }

    public function pushMessage($code, $params = []) : MessageInterface
    {
        return $this->sendMessage($this->buildMessage($code, $params));
    }

    public function sendMessage(MessageInterface $message) : MessageInterface
    {
        foreach($this->handler as $handler) {
            $handler->handle($message);
        }
        return $message;
    }
}
